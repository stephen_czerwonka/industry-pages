var gulp = require('gulp');
var less = require('gulp-less');
var hs = require('./hubspot-api/main.js');
var request = require('sync-request');
var fs = require('fs');
var util = require('util');
var git = require('gulp-git');


var jsEscape = require('gulp-js-escape');

var express = require('express');
var app = express() //express server  
var server = require('http').createServer(app) //http wrapper server  
var io = require('socket.io').listen(server); //socket.io in charge  
server.listen(process.env.PORT);

app.get('/refresh', function(req,res){
    io.sockets.emit('reload');
})

var saveCount = 0;


gulp.task('watch', function() {
    gulp.watch('./css/*.less', ['lessify']);
    gulp.watch('./css/industry-styles.css', ['liveUpdate']);

});

gulp.task('liveUpdate', function() {
    var cssFile = fs.readFileSync('./css/industry-styles.css', 'utf8');
    io.sockets.emit('reload', {
        css: cssFile
    });
    
    saveCount++
    
    if(saveCount > 2){
        // var res = hs.templates.update(4120657415, cssFile);
        // console.log(res.statusCode);
        // gulp.src('./css/*')
        //     .pipe(git.add())
        //     .pipe(git.commit(['initial commit', 'save']));
        //     git.push('origin', 'master', function (err) {
        //     if (err) throw err;
        //   });
        saveCount = 0;
    }
    
});


gulp.task('uploadStyle', function() {
    var cssFile = fs.readFileSync('./css/industry-styles.css', 'utf8');
    var res = hs.templates.update(4120657415, cssFile);
    console.log(res.statusCode);
    
    
});


gulp.task('lessify', function() {

    gulp.src('./css/industry-styles.less')
        .pipe(less())
        .pipe(gulp.dest('css'));

});


gulp.task('gitcommit', function(){
  return gulp.src('./css/*')
    .pipe(git.add())
    .pipe(git.commit(['initial commit', 'save']));
});