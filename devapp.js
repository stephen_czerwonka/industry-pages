var express = require('express');
var app = express() //express server  
var server = require('http').createServer(app) //http wrapper server  
var io = require('socket.io').listen(server); //socket.io in charge  
server.listen(process.env.PORT);

app.get('/refresh', function(req,res){
    io.sockets.emit('reload');
    res.send('refreshing...');
    console.log('emit');
})


//set up express to serve static content
// app.use(express.directory(__dirname));  
app.use(express.static('public'));
